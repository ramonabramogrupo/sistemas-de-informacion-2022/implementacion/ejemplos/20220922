﻿USE empresas;

/**
  Consulta 1
  Lista todos los registros y campos
  de la tabla personas
  */
SELECT 
    *  -- campos
  FROM 
    personas -- tabla o tablas
    p -- alias de la tabla
    ;
/**
  Consulta 2
  LISTAR EL ID, NOMBRE Y APELLIDOS DE LAS
  PERSONAS
  */

  SELECT 
    id, 
    nombre, 
    apellidos  -- los campos a mostrar
  FROM 
    personas p; -- tabla

  /**
    CONSULTA 3
    Listame los nombres de las personas (sin repetidos)
  */
    
    SELECT 
        DISTINCT -- eliminar los datos repetidos de la consulta
        p.nombre 
      FROM 
        personas p;

  /** 
    CONSULTA 4
    Listame los nombres de las personas (sin repetidos)
    **/

    SELECT DISTINCT 
        empresas.p.nombre 
      FROM 
        empresas.personas p;

  /** 
    consulta 5
    listar el nombre y la poblacion de las personas
   **/

    SELECT DISTINCT
        p.nombre n, -- colocando un alias al campo nombre
        p.poblacion po -- colocando un alias al campo poblacion
      FROM 
        personas p;

/**
  Consulta 6
  MOSTRAR EL NOMBRE DE LAS EMPRESAS
**/

  SELECT DISTINCT
      e.nombre 
    FROM 
      empresas e;

  /**
    Consulta 7
    MOSTRAR LA POBLACION DE LAS EMPRESAS
    **/

    SELECT DISTINCT
        e.poblacion 
      FROM 
        empresas e;

   /**
     Consulta 8
     MOSTRAR EL NOMBRE DE LAS PERSONAS
   **/

    SELECT DISTINCT 
      p.nombre 
    FROM 
      personas p;

   /**
     Consulta 9
     MOSTRAR LA FECHA Y EL NOMBRE DE LAS PERSONAS
    **/

    SELECT DISTINCT 
        p.nombre,p.fecha 
      FROM 
        personas p;

/**
  Consulta 10
  MOSTRAR EL CODIGO DE LAS EMPRESAS QUE TIENEN TRABAJADORES
 **/

  SELECT DISTINCT 
      p.empresa 
    FROM 
      personas p;

  /**
    Consulta 11
    nombre de los trabajadores cuya empresa es la 2
    **/

    SELECT DISTINCT
          p.nombre 
        FROM 
          personas p
        WHERE
          p.empresa=2;

   /** 
    cONSULTA 12 
    NOMBRE E ID DE LOS TRABAJADORES CUYA EMPRESA 
    ES LA 3
    **/
    
    SELECT 
        p.id, p.nombre 
      FROM 
        personas p
      WHERE
        p.empresa=3;


/**
CONSULTA 13
LISTAME NOMBRE Y APELLIDOS DE LAS PERSONAS JUNTO 
CON LA SUMA DE LOS DATO1,DATO2,DATO3
**/

    SELECT 
        p.nombre,
        p.apellidos,
        p.dato1+p.dato2+p.dato3 AS suma
      FROM 
        personas p;

/** 
  Consulta 14
  LISTAR NOMBRE Y APELLIDOS DE LAS PERSONAS
  CUYA SUMA DEL DATO1,DATO2,DATO3 ES MAYOR QUE 20
**/
      
SELECT 
    p.nombre,p.apellidos
  FROM 
    personas p
  WHERE
    (p.dato1+p.dato2+p.dato3)>20;


/** 
  Consulta 15
  LISTAR NOMBRE,APELLIDOS y LA SUMA DE LOS DATO1,DATO2,DATO3 
  DE LAS PERSONAS CUYA SUMA DEL DATO1,DATO2,DATO3 ES MAYOR QUE 20
**/

  SELECT 
      p.nombre,
      p.apellidos,
      p.dato1+p.dato2+p.dato3 AS suma 
    FROM 
      personas p
    WHERE 
      (p.dato1+p.dato2+p.dato3)>20;


  