﻿USE empresas;
SELECT * FROM personas p;
SELECT * FROM empresas e;


  -- DATO MAYOR DE LOS 3 POR PERSONA
  SELECT GREATEST(p.dato1,p.dato2,p.dato3) mayor 
    FROM personas p;

  -- el dato1 mayor de todas las personas
  SELECT max(p.dato1) 
    FROM personas p;

  -- indica el numero de poblaciones donde hay empresas

    # opcion1
  SELECT COUNT(DISTINCT e.poblacion) 
    FROM empresas e;

    # opcion2
  SELECT COUNT(*) FROM   
  (SELECT DISTINCT e.poblacion FROM empresas e) c1;

-- En cuantas poblaciones viven personas
  
  # opcion1
  SELECT COUNT(DISTINCT poblacion) FROM personas p;

  #opcion2

  -- c1
  -- poblaciones donde viven personas sin repetidos
  SELECT DISTINCT p.poblacion FROM personas p;

  -- consulta final
  SELECT COUNT(*) 
    FROM (SELECT DISTINCT p.poblacion FROM personas p) c1;


  -- MEDIA DEL DATO1, DATO2, DATO3 DE TODAS LAS PERSONAS 
  SELECT 
    AVG(p.dato1) media1,
    AVG(p.dato2) media2,
    AVG(p.dato3) media3

    FROM personas p;

  -- LA MEDIA DE LOS 3 DATOS POR PERSONA
  SELECT p.id,p.nombre,(p.dato1+p.dato2+p.dato3)/3 media
    FROM personas p;

  -- MEDIA DE LAS NOTAS MEDIAS
    
    SELECT AVG(C1.media) 
      FROM 
      (SELECT (p.dato1+p.dato2+p.dato3)/3 media
        FROM personas p) C1;

    SELECT AVG((p.dato1+p.dato2+p.dato3)/3) 
      FROM personas p;

    -- EL NUMERO DE TRABAJADORES POR POBLACION

      SELECT 
          SUM(e.trabajadores),e.poblacion 
        FROM empresas e
        GROUP BY e.poblacion;
        

        -- numero de trabajadores y numero de empresas por poblacion
        SELECT 
          COUNT(*) numeroEmpresas, 
          SUM(e.trabajadores) numeroTrabajadores,
          e.poblacion 
        FROM empresas e
        GROUP BY e.poblacion;

        -- cuantas personas han empezado a trabajar 
        -- en las empresas por año

        SELECT YEAR(p.fechaTrabaja) anno,COUNT(*) numero 
          FROM personas p 
          GROUP BY YEAR(p.fechaTrabaja);

        SELECT YEAR(p.fechaTrabaja) anno,COUNT(*) numero 
          FROM personas p 
          GROUP BY anno;
      
-- NUMERO DE PERSONAS POR POBLACION QUE HAYAN 
-- NACIDO EN NOVIEMBRE
SELECT 
  p.poblacion,
  COUNT(*) numeroPersonas 
  FROM personas p 
  WHERE MONTH(p.fecha)=11
  GROUP BY p.poblacion;

-- nombre de las poblaciones que tienen mas de 1 persona
  SELECT 
      p.poblacion,
      COUNT(*) numeroPersonas 
    FROM personas p 
    GROUP BY p.poblacion
    HAVING numeroPersonas>1;

  -- realizar el having sin having
SELECT 
  * 
    FROM 
      (SELECT p.poblacion,COUNT(*) numeroPersonas 
        FROM personas p 
        GROUP BY p.poblacion
      ) c1
    WHERE c1.numeroPersonas>1;
